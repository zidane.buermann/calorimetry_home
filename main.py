from functions import m_json
from functions import m_pck

path_heat_cap = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
path_newton = "/home/pi/calorimetry_home/datasheets/setup_newton.json"

#Zur Durchführung eines Versuchs müssen einfach die jeweiligen Funktionen mit dem gewünschten Pfad auskommentiert werden, wie es unten #exemplarisch mit der Wärmekapazität zu sehen ist

#Sensoren checken
m_pck.check_sensors()

#metadata_newton = m_json.get_metadata_from_setup(path_newton)
metadata_setup_heat_cap = m_json.get_metadata_from_setup(path_heat_cap)

#Den JSON Dateien die Seriennummern der Sensoren hinzufügen
#m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata_newton)
m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata_setup_heat_cap)

#Die JSON Dateien der Setups in einen neuen Ordner 'archive' ablegen
#m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_newton, '/home/pi/calorimetry_home/archive/Newton_Experiment')
m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_heat_cap, '/home/pi/calorimetry_home/archive/Heat_Capacity_Experiment')

#Messvorgang starten und Messdaten erfassen
#data_newton = m_pck.get_meas_data_calorimetry(metadata_newton)
data_setup_heat_cap = m_pck.get_meas_data_calorimetry(metadata_setup_heat_cap)

#H5 Datei mit den Messdaten zum jeweiligen Sensor erstellen
#m_pck.logging_calorimetry(data_newton, metadata_newton, '/home/pi/calorimetry_home/archive/Newton_Experiment', '/home/pi/calorimetry_home/datasheets')
m_pck.logging_calorimetry(data_setup_heat_cap, metadata_setup_heat_cap, '/home/pi/calorimetry_home/archive/Heat_Capacity_Experiment', '/home/pi/calorimetry_home/datasheets')


